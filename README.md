**German**

**Eine DIY Wetterstation auf Basis eines Raspberry Pi**

Vollständige Instruktionen zum Einrichten der Hardware und Installieren der Software befinden sich in der beiliegenden Dokumentation.
Ihr braucht:
*  Raspberry Pi ab Version 2B
*  Mehrere Sensoren aus dem [Sunfounder Sensor Kit for Raspberry B+](https://www.sunfounder.com/learn/category/sensor-kit-v2-0-for-raspberry-pi-b-plus.html)
*  Einen [Eltako Windsensor WS](https://www.amazon.de/Eltako-Windsensor-WS/dp/B0018LBFG8)
*  Sowie mehrere Jumperkabel und ein Breadbord

Dies ist ein Projekt als Teil der schulischen Ausbildung zum Informationstechnischen Assisten (ITA) des Robert-Bosch Berufskollegs in Dortmund.

Zum Installieren...

Die Daten im Ordner "Backend" auf ein beliebiges Benutzerverzeichnis auf dem Raspberry kopieren und ein crontab im folgenden Format installieren:<br>
**" * * * * * java -jar /pfad/zur/Ordner/readPython/dist/readPython.jar "**<br>
**" 5 * * * * java -jar /pfad/zur/Ordner/statsHour/dist/StatsHour.jar h "**<br>
**" 30 0 * * * java -jar /pfad/zur/Ordner/statsHour/dist/StatsHour.jar d "**<br>

Die Pythonscript müssen im Ordner **/home/pi/pythonScripts/** abgelegt werden.
Die Datenbank lässt sich in phpMyAdmin mit der Datei im Ordner Datenbanken importieren.

Der Inhalt des Ordners Frontend ist in den Ordner /var/www/html zu kopieren. Ein Webserver (z.B. Apache2) ist für die Anzeige der Website notwendig.


Für mehr Details, liest die Dokumentation.
