#!/usr/bin/env python
import PCF8591 as ADC
import RPi.GPIO as GPIO
import time
import math

DO = 21
GPIO.setmode(GPIO.BCM)

def setup():
        ADC.setup(0x48)
        GPIO.setup(DO, GPIO.IN)

def Print(x):
        if x != 0:
                print '0'
        else:
                print '1'

def read():
        status = 1
       #print ADC.read(0)
        tmp = GPIO.input(DO);
        Print(tmp)


if __name__ == '__main__':
        try:
                setup()
                read()
        except KeyboardInterrupt:
                pass

