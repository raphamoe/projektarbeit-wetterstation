import RPi.GPIO as GPIO
import time, math

pin = 20
count = 0
r_cm = 5.2

def calculate_speed(r_cm, time_sec):
    global count
    circ_cm = (2 * math.pi) * r_cm
    rot = count / 2.0
    dist_km = (circ_cm * rot) / 100000.0 # umwandeln in km
    km_per_sec = dist_km / time_sec
    km_per_hour = km_per_sec * 3600 # umwandeln in Entfernung/Stunde
    return km_per_hour

def spin(channel):
    global count
    count += 1
   # print (count)

GPIO.setmode(GPIO.BCM) # GPIO als Nummer
GPIO.setup(pin, GPIO.IN, GPIO.PUD_UP)
GPIO.add_event_detect(pin, GPIO.FALLING, callback=spin)

interval = 5 # Messintervall Sek.

#while True:
count = 0
time.sleep(interval)
print calculate_speed(r_cm,interval)
