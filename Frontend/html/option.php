<!DOCTYPE html>
<html lang="en">
    <?php include './php/database.php'; ?>
    <?php include './php/live.php'; ?>
    <?php include './php/cookie.php'; ?>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="refresh" content="300">
        <title>Wetterstation - Info</title>
        <link href="./style/style.css" type="text/css" rel="stylesheet">
        <script src="./js/Script.js"></script>
        <script>
          function loadSelect(){
            var tempCookie = <?php echo getCookieTemp(); ?>;
            var presCookie = <?php echo getCookiePres(); ?>;
            var speedCookie = <?php echo getCookieSpeed(); ?>;

            if(tempCookie == "fahr"){
              document.getElementById('tempunit').value="fahr";
            }else if (tempCookie == "kel") {
              document.getElementById('tempunit').value="kel";
            }else{
              document.getElementById('tempunit').value="cel";
            }

            if(presCookie == "bar"){
              document.getElementById('presunit').value="bar";
            }else if (presCookie == "pa") {
              document.getElementById('presunit').value="pa";
            }else{
              document.getElementById('presunit').value="hpa";
            }

            if(speedCookie == "mph"){
              document.getElementById('speedunit').value="mph";
            }else if (speedCookie == "knoten") {
              document.getElementById('speedunit').value="knoten";
            }else{
              document.getElementById('speedunit').value="kmh";
            }
          }
        </script>
    </head>

    <body onload="getDate(), getHeadLiveData(), loadSelect()">
        <div class="navigation">
            <img id="logo" src="./img/logo.png" />
            <div class="selectArea">
                <a href="index.php"><img class="rotate" src="./img/icons/home_50.png" alt="Home"></a>
                <div class="drop"><img class="rotate" src="./img/icons/diagram_50.png" alt="Statistics"></img>
                    <div class="dropdown">
                      <a class="dropdown-content" href="statisticWeek.php" title="Daten der letzen 7 Tage">Woche</a><br>
                      <a class="dropdown-content" href="statisticDay.php" title="Daten der letzen 24 Stunden">Tag</a><br>
                      <a class="dropdown-content" href="statisticCustom.php" title="Daten in Stunden selber auswählen">Custom</a><br>
                    </div>
                </div>
                <a class="selected" href="option.php"><img class="rotate" src="./img/icons/gear_50.png" alt="Informations"></a>
            </div>
            <div class="stats">
                <p id="time"></p>
                <p id="temp"></p>
            </div>
        </div>
        <div class="main">
          <div class="option-div">
            <form action="./option.php" method="post">
              <table>
                <tr>
                  <td>
                    Temperatur:
                  </td>
                  <td>
                    <select class="option-data" name="tempunit" id="tempunit">
                      <option value="cel">Celsius</option>
                      <option value="fahr">Fahrenheit</option>
                      <option value="kel">Kelvin</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>
                    Luftdruck:
                  </td>
                  <td>
                    <select class="option-data" name="presunit" id="presunit">
                      <option value="hpa">Hekto Pascal</option>
                      <option value="pa">Pascal</option>
                      <option value="bar">Bar</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>
                    Windgeschwindigkeit:
                  </td>
                  <td>
                    <select class="option-data" name="speedunit" id="speedunit">
                      <option value="kmh">km/h</option>
                      <option value="mph">mph</option>
                      <option value="knoten">Knoten</option>
                    </select>
                  <td>
                </tr>
              </table>
            <button type="submit">Speichern</button>
          </form>
          </div>
        </div>
    </body>
</html>
