<!DOCTYPE html>
<html lang="en">
    <?php include './php/database.php'; ?>
    <?php include './php/live.php'; ?>
    <head>
        <meta charset="utf-8">
        <title>Wetterstation - Custom</title>
        <link href="./style/style.css" type="text/css" rel="stylesheet">
        <script src="./js/Script.js"></script>
        <script src="./js/Chart.js"></script>
    </head>

    <body onload="getDate(), getHeadLiveData()">
        <div class="navigation">
            <img id="logo" src="./img/logo.png" />
            <div class="selectArea">
                <a href="index.php"><img class="rotate" src="./img/icons/home_50.png" alt="Home"></a>
                <div class="drop selected"><img class="rotate" src="./img/icons/diagram_50.png" alt="Statistics"></img>
                    <div class="dropdown">
                        <a class="dropdown-content" href="statisticWeek.php" title="Daten der letzen 7 Tage">Woche</a><br>
                        <a class="dropdown-content" href="statisticDay.php" title="Daten der letzen 24 Stunden">Tag</a><br>
                        <a class="dropdown-content" href="statisticCustom.php" title="Daten in Stunden selber auswählen">Custom</a><br>
                    </div>
                </div>
                <a href="option.php"><img class="rotate" src="./img/icons/gear_50.png" alt="Informations"></a>
            </div>
            <div class="stats">
                <p id="time"></p>
                <p id="temp"></p>
            </div>
        </div>
        <div class="main">
            <h2>Selbstausgewählte Daten</h2>
            <p>Der linke Kalender selektiert das Startdatum, der Rechte das Enddatum<br>Muster: TT.MM.JJJJ</p>
            <form method="POST" action="statisticCustom.php">
                <div class="center-dateselector">
                    <input class="dateSelecter" type="date" name="beginn">
                    <input class="dateSelecter" type="date" name="end">
                    <button class="dateBtn"type="submit">Search</button>
                    <div>
                        </form>
                        <div>
                            <div class="leftDia"><canvas id="temperature"></canvas></div>
                            <div class="rightDia"><canvas id="velocity"></canvas></div>
                            <div class="leftDia"><canvas id="humidity"></canvas></div>
                            <div class="rightDia"><canvas id="pressure"></canvas></div>
                            <div class="leftDia"><canvas id="rain"></canvas></div>
                        </div>
                    </div>

                    <script>
                        var cookieTemp = <?php echo getCookieTemp(); ?>;
                        var cookiePres = <?php echo getCookiePres(); ?>;
                        var cookieSpeed = <?php echo getCookieSpeed(); ?>;
                        var labels = <?php
        if (isset($_POST["beginn"])) {
             echo getLabelCustom();
        } else {
            echo getLabelCustom1();
        }
        ?>;
                        var dataAvgTemp = <?php
        if (isset($_POST["beginn"])) {
             echo getCustomAvgTemp();
        } else {
            echo getCustomAvgTempS();
        }
        ?>;
                        var dataMaxTemp = <?php
        if (isset($_POST["beginn"])) {
             echo getCustomMaxTemp();
        } else {
            echo getCustomMaxTempS();
        }
        ?>;
                        var dataMinTemp = <?php
        if (isset($_POST["beginn"])) {
             echo getCustomMinTemp();
        } else {
            echo getCustomMinTempS();
        }
        ?>;
                        var dataSpeed = <?php
        if (isset($_POST["beginn"])) {
             echo getCustomSpeed();
        } else {
            echo getCustomSpeedS();
        }
        ?>;
                        var dataHumi = <?php
        if (isset($_POST["beginn"])) {
             echo getCustomHum();
        } else {
            echo getCustomHumS();
        }
        ?>;
                        var dataPres = <?php
        if (isset($_POST["beginn"])) {
             echo getCustomPres();
        } else {
            echo getCustomPresS();
        }
        ?>;
                        var dataRain = <?php
        if (isset($_POST["beginn"])) {
             echo getCustomRain();
        } else {
            echo getCustomRainS();
        }
        ?>;
                        var canvasTemp = document.getElementById('temperature').getContext('2d');
                        var canvasSpeed = document.getElementById('velocity').getContext('2d');
                        var canvasHumi = document.getElementById('humidity').getContext('2d');
                        var canvasPres = document.getElementById('pressure').getContext('2d');
                        var canvasRain = document.getElementById('rain').getContext('2d');
                        var unitTemp;
                        var unitSpeed;
                        var unitSpeed;
                        if (cookieTemp == "fahr") {
                            unitTemp = "F";
                            for (var i = 0; i < dataAvgTemp.length; i++) {
                                dataAvgTemp[i] = parseFloat(dataAvgTemp[i]) * 9 / 5 + 32;
                                dataMaxTemp[i] = parseFloat(dataMaxTemp[i]) * 9 / 5 + 32;
                                dataMinTemp[i] = parseFloat(dataMinTemp[i]) * 9 / 5 + 32;
                            }
                        } else if (cookieTemp == "kel") {
                            unitTemp = "K";
                            for (var i = 0; i < dataAvgTemp.length; i++) {
                                dataAvgTemp[i] = parseFloat(dataAvgTemp[i]) + parseFloat(273.15);
                                dataMaxTemp[i] = parseFloat(dataMaxTemp[i]) + parseFloat(273.15);
                                dataMinTemp[i] = parseFloat(dataMinTemp[i]) + parseFloat(273.15);
                            }
                        } else {
                            unitTemp = "C";
                        }

                        if (cookiePres == "bar") {
                            unitPres = "Bar";
                            for (var i = 0; i < dataPres.length; i++) {
                                dataPres[i] = parseInt(dataPres[i]) / 100000;
                            }
                        } else if (cookieTemp == "pa") {
                            unitPres = "Pa";
                        } else {
                            unitPres = "hPa";
                            for (var i = 0; i < dataPres.length; i++) {
                                dataPres[i] = parseInt(dataPres[i]) / 100;
                            }
                        }

                        if (cookieSpeed == "mph") {
                            unitSpeed = "mph";
                            for (var i = 0; i < dataSpeed.length; i++) {
                                dataSpeed[i] = parseInt(dataSpeed[i]) * 0.621371;
                            }
                        } else if (cookieSpeed == "knoten") {
                            unitSpeed = "kmn";
                            for (var i = 0; i < dataSpeed.length; i++) {
                                dataSpeed[i] = parseInt(dataSpeed[i]) * 0.539957;
                            }
                        } else {
                            unitSpeed = "km/h";
                        }

                        var optionsTemp = {
                            responsive: true,
                            title: {
                                display: true,
                                text: 'Lufttemperatur'
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                            },
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            },
                            scales: {
                                xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Datum'
                                        }
                                    }],
                                yAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Temperatur in °' + unitTemp
                                        }
                                    }]
                            }
                        };

                        var optionsHumid = {
                            responsive: true,
                            title: {
                                display: true,
                                text: 'Luftfeuchtigkeit'
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                            },
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            },
                            scales: {
                                xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Datum'
                                        }
                                    }],
                                yAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Luftfeuchtigkeit in %'
                                        }
                                    }]
                            }
                        };

                        var optionsPress = {
                            responsive: true,
                            title: {
                                display: true,
                                text: 'Luftdruck'
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                            },
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            },
                            scales: {
                                xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Datum'
                                        }
                                    }],
                                yAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Luftdruck in ' + unitPres
                                        }
                                    }]
                            }
                        };

                        var optionsWind = {
                            responsive: true,
                            title: {
                                display: true,
                                text: 'Windgeschwindigkeit'
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                            },
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            },
                            scales: {
                                xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Datum'
                                        }
                                    }],
                                yAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Windgeschwindigkeit in ' + unitSpeed
                                        }
                                    }]
                            }
                        };

                        var optionsRain = {
                            responsive: true,
                            title: {
                                display: true,
                                text: 'Regen'
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                            },
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            },
                            scales: {
                                xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Datum'
                                        }
                                    }],
                                yAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Anzahl der Regenminuten'
                                        }
                                    }]
                            }
                        };

                        new Chart(canvasTemp, {
                            // The type of chart we want to create
                            type: 'line',
                            // The data for our dataset
                            data: {
                                labels: labels,
                                datasets: [{
                                        cubicInterpolationMode: 'monotone',
                                        label: "Min. emperatur",
                                        borderColor: '#204790',
                                        data: dataMinTemp,
                                        fill: false,
                                    }, {
                                        cubicInterpolationMode: 'monotone',
                                        label: "Max. Temperatur",
                                        borderColor: '#D3000C',
                                        data: dataMaxTemp,
                                        fill: false,
                                    }, {
                                        cubicInterpolationMode: 'monotone',
                                        label: "Avg. Temperatur",
                                        borderColor: "#3cbc25",
                                        data: dataAvgTemp,
                                        fill: false,
                                    }]
                            },
                            // Configuration options go here
                            options: optionsTemp
                        });
                        new Chart(canvasSpeed, {
                            // The type of chart we want to create
                            type: 'line',
                            // The data for our dataset
                            data: {
                                labels: labels,
                                datasets: [{
                                        label: "Windgeschwindigkeit",
                                        borderColor: '#204790',
                                        data: dataSpeed,
                                    }]
                            },
                            // Configuration options go here
                            options: optionsWind
                        });
                        new Chart(canvasPres, {
                            // The type of chart we want to create
                            type: 'line',
                            // The data for our dataset
                            data: {
                                labels: labels,
                                datasets: [{
                                        label: "Luftdruck",
                                        borderColor: '#204790',
                                        data: dataPres,
                                    }]
                            },
                            // Configuration options go here
                            options: optionsPress
                        });
                        new Chart(canvasHumi, {
                            // The type of chart we want to create
                            type: 'line',
                            // The data for our dataset
                            data: {
                                labels: labels,
                                datasets: [{
                                        label: "Luftfeuchtigkeit",
                                        borderColor: '#204790',
                                        data: dataHumi,
                                    }]
                            },
                            // Configuration options go here
                            options: optionsHumid
                        });
                        new Chart(canvasRain, {
                            // The type of chart we want to create
                            type: 'bar',
                            // The data for our dataset
                            data: {
                                labels: labels,
                                datasets: [{
                                        label: "Regen",
                                        backgroundColor: '#2222BB',
                                        borderColor: '#204790',
                                        data: dataRain,
                                    }]
                            },
                            // Configuration options go here
                            options: optionsRain
                        });
                    </script>
                </div>
                </body>

                </html>
