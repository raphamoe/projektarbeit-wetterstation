<!DOCTYPE html>
<html lang="en">
    <?php include './php/database.php'; ?>
    <?php include './php/live.php'; ?>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="refresh" content="300">
        <title>Wetterstation - Home</title>
        <link href="./style/style.css" type="text/css" rel="stylesheet">
        <script src="./js/Script.js"></script>
        <script src="./js/Chart.js"></script>
    </head>

    <body onload="getDate(), getLiveData(), getHeadLiveData()">
        <div class="navigation">
            <img id="logo" src="./img/logo.png" />
            <div class="selectArea">
                <a class="selected" href="index.php"><img class="rotate" src="./img/icons/home_50.png" alt="Home"></a>
                <div class="drop"><img class="rotate" src="./img/icons/diagram_50.png" alt="Statistics">
                    <div class="dropdown">
                        <a class="dropdown-content" href="statisticWeek.php" title="Daten der letzen 7 Tage">Woche</a><br>
                        <a class="dropdown-content" href="statisticDay.php" title="Daten der letzen 24 Stunden">Tag</a><br>
                        <a class="dropdown-content" href="statisticCustom.php" title="Daten in Stunden selber auswählen">Custom</a><br>
                    </div>
                </div>
                <a href="option.php"><img class="rotate" src="./img/icons/gear_50.png" alt="Informations"></a>
            </div>
            <div class="stats">
                <p id="time" onclick="getLiveData()"></p>
                <p id="temp"></p>
            </div>
        </div>
        <div class="main">
            <h2>Willkommen auf der Startseite</h2>
            <div class="livedata">
                <div style="color: #3d5ce2;">Live</div>
                <div id="liveTemp">Temperatur:</div>
                <div id="livePressure">Luftdruck:</div>
                <div id="liveHumidity">Luftfeuchtigkeit:</div>
                <div id="liveSpeed">Windgeschwindigkeit:</div>
                <div style="font-size: 12px;">Daten werden minütlich aktualisiert</div>
            </div>
            <div class="text-div">
                Weatherpi ist eine Statistik-Webseite die die aktuellen und vergengenden Wetterdaten in Statisken darstellt.<br>
                Die Daten werden aus einer Datenbank ausgelesen, die von einer, auf einem Raspberry-Pi basierenden Wetterstation<br>
                basiert. An dem Raspberry-Pi sind Sensoren angeschlossen die die Temperatur, den Luftdruck, die Luftfeuchtigkeit<br>
                die Windgschwindigkeit und ein Feutigkeitssensor, der ein Signal zurückgibt falls es regnet, auslesen.
            </div>
        </div>
    </body>

</html>
