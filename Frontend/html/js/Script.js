function repeater() {
  var text = "Test";
  var result = "";
  for (var i = 0; i < 50; i++) {
    result += text + "<br>";
  }
  document.getElementById('rep').innerHTML = result;
}

function getDate() {
  var d = new Date();
  var hour = "";
  var min = "";
  if (d.getHours() < 10) {
    hour += "0";
  }
  if (d.getMinutes() < 10) {
    min += "0";
  }
  hour += d.getHours();
  min += d.getMinutes();
  document.getElementById('time').innerHTML = hour + ":" + min;
}
setInterval(getDate, 3000);
