<?php
function con() {
    $hostname = "localhost";
    $username = "admin";
    $password = "ProjektSQL";
    $db = "wetter";
    $link = mysqli_connect($hostname, $username, $password, $db);
    if (!$link) {
        echo "Fehler: konnte nicht mit MySQL verbinden." . PHP_EOL;
        echo "Debug-Fehlernummer: " . mysqli_connect_errno() . PHP_EOL;
        echo "Debug-Fehlermeldung: " . mysqli_connect_error() . PHP_EOL;
        exit;
    }
    return $link;
}

  function getEnd(){
    $end = $_POST["end"];
    $endstamp = strtotime($end);
    return $endstamp;
  }

  function getBeginn(){
    $beginn = $_POST["beginn"];
    $begstamp = strtotime($beginn);
    return $begstamp;
  }

  function getCustomAvgTemp() {
      $link = con();
      $query = 'SELECT Temperatur_AVG FROM wetter_h WHERE Time BETWEEN ' . getBeginn() . ' AND ' . getEnd();
      $ausgabe = array();
      /* Select queries return a resultset */
      if ($result = mysqli_query($link, $query)) {
          while ($row = mysqli_fetch_array($result)) {
              $ausgabe[] = $row['Temperatur_AVG'];
          }
          mysqli_free_result($result);
      }
      mysqli_close($link);
      return json_encode($ausgabe);
  }

  function getCustomTime() {
      $link = con();
      $query = 'SELECT Time FROM wetter_h WHERE Time BETWEEN ' . getBeginn() . ' AND ' . getEnd();
      $ausgabe = array();
      /* Select queries return a resultset */
      if ($result = mysqli_query($link, $query)) {
          while ($row = mysqli_fetch_array($result)) {
              $ausgabe[] = date('d.m H:00',$row['Time']);
          }
          mysqli_free_result($result);
      }
      mysqli_close($link);
      return json_encode($ausgabe);
  }
 ?>
