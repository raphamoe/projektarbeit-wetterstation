<?php
$time = time() + 60 * 60 * 24 * 30;

if(!isset($_COOKIE["temp"])){
  setcookie("temp", "cel", $time);
}else if(isset($_POST["tempunit"])){
  setcookie("temp", $_POST["tempunit"], $time);
}

if(!isset($_COOKIE["speed"])){
  setcookie("speed", "kmh", $time);
}else if(isset($_POST["speedunit"])){
  setcookie("speed", $_POST["speedunit"], $time);
}

if(!isset($_COOKIE["pres"])){
  setcookie("pres", "hpa", $time);
}else if(isset($_POST["presunit"])){
  setcookie("pres", $_POST["presunit"], $time);
}

unset($_POST["presunit"]);
unset($_POST["speedunit"]);
unset($_POST["tempunit"]);
?>
