<?php

//-----------------Connection--------------
function getConnection() {
    $hostname = "localhost";
    $username = "admin";
    $password = "ProjektSQL";
    $db = "wetter";
    $link = mysqli_connect($hostname, $username, $password, $db);
    if (!$link) {
        echo "Fehler: konnte nicht mit MySQL verbinden." . PHP_EOL;
        echo "Debug-Fehlernummer: " . mysqli_connect_errno() . PHP_EOL;
        echo "Debug-Fehlermeldung: " . mysqli_connect_error() . PHP_EOL;
        exit;
    }
    return $link;
}

//---------------------Data---------------------
function getData($table, $sel, $where) {
    $link = getConnection();
    $query = 'SELECT * FROM ' . $table . $where;
    $ausgabe = array();
    /* Select queries return a resultset */
    if ($result = mysqli_query($link, $query)) {
        while ($row = mysqli_fetch_array($result)) {
            $ausgabe[] = $row[$sel];
        }
        mysqli_free_result($result);
    }
    mysqli_close($link);
    return json_encode($ausgabe);
}

//--------------------Livedata-----------------------------------
function getLastTemp() {
    $link = getConnection();
    $query = 'SELECT Temperatur FROM wetter ORDER BY ID DESC LIMIT 1';
    $ausgabe;
    /* Select queries return a resultset */
    if ($result = mysqli_query($link, $query)) {
        while ($row = mysqli_fetch_array($result)) {
            $ausgabe = $row['Temperatur'];
        }
        mysqli_free_result($result);
    }
    mysqli_close($link);
    return json_encode($ausgabe);
}

function getLastPres() {
    $link = getConnection();
    $query = 'SELECT Luftdruck FROM wetter ORDER BY ID DESC LIMIT 1';
    $ausgabe;
    /* Select queries return a resultset */
    if ($result = mysqli_query($link, $query)) {
        while ($row = mysqli_fetch_array($result)) {
            $ausgabe = $row['Luftdruck'];
        }
        mysqli_free_result($result);
    }
    mysqli_close($link);
    return json_encode($ausgabe);
}

function getLastHum() {
    $link = getConnection();
    $query = 'SELECT Luftfeuchtigkeit FROM wetter ORDER BY ID DESC LIMIT 1';
    $ausgabe;
    /* Select queries return a resultset */
    if ($result = mysqli_query($link, $query)) {
        while ($row = mysqli_fetch_array($result)) {
            $ausgabe = $row['Luftfeuchtigkeit'];
        }
        mysqli_free_result($result);
    }
    mysqli_close($link);
    return json_encode($ausgabe);
}

function getLastSpeed() {
    $link = getConnection();
    $query = 'SELECT Windgeschwindigkeit FROM wetter ORDER BY ID DESC LIMIT 1';
    $ausgabe;
    /* Select queries return a resultset */
    if ($result = mysqli_query($link, $query)) {
        while ($row = mysqli_fetch_array($result)) {
            $ausgabe = $row['Windgeschwindigkeit'];
        }
        mysqli_free_result($result);
    }
    mysqli_close($link);
    return json_encode($ausgabe);
}

//---------------------Labels---------------------------------------
function getLabelHour() {
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour) - 3600; //3600 = 1h;
    $hourStmp2 = $hourStmp - 86400; //86400 = 24h
    $link = getConnection();
    $query = 'SELECT Time FROM  wetter_h WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    $ausgabe = array();
    /* Select queries return a resultset */
    if ($result = mysqli_query($link, $query)) {
        while ($row = mysqli_fetch_array($result)) {
            $ausgabe[] = $row['Time'];
        }
        mysqli_free_result($result);
    }
    for ($i = 0; $i < count($ausgabe); $i++) {
        $ausgabe[$i] = date('H:00', $ausgabe[$i]);
    }
    mysqli_close($link);
    return json_encode($ausgabe);
}

function getLabelWeek() {
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour) - 86400;
    $hourStmp2 = $hourStmp - 60 * 60 * 24 * 7;
    $link = getConnection();
    $query = 'SELECT Time FROM  wetter_d WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    $ausgabe = array();
    /* Select queries return a resultset */
    if ($result = mysqli_query($link, $query)) {
        while ($row = mysqli_fetch_array($result)) {
            $ausgabe[] = $row['Time'];
        }
        mysqli_free_result($result);
    }
    for ($i = 0; $i < count($ausgabe); $i++) {
        $ausgabe[$i] = date('d.m.y', $ausgabe[$i]);
    }
    mysqli_close($link);
    return json_encode($ausgabe);
}

function getEnd() {
    $end = $_POST["end"];
    $endstamp = strtotime($end);
    return $endstamp;
}

function getBeginn() {
    $beginn = $_POST["beginn"];
    $begstamp = strtotime($beginn);
    return $begstamp;
}

function getLabelCustom() {
    $link = getConnection();
    $query = 'SELECT Time FROM wetter_h WHERE Time BETWEEN ' . getBeginn() . ' AND ' . getEnd();
    $ausgabe = array();
    /* Select queries return a resultset */
    if ($result = mysqli_query($link, $query)) {
        while ($row = mysqli_fetch_array($result)) {
            $ausgabe[] = date('d.m H:00', $row['Time']);
        }
        mysqli_free_result($result);
    }
    mysqli_close($link);
    return json_encode($ausgabe);
}

function getLabelCustom1(){
    $link = getConnection();
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24;
    $query = 'SELECT Time FROM wetter_h WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    $ausgabe = array();
    /* Select queries return a resultset */
    if ($result = mysqli_query($link, $query)) {
        while ($row = mysqli_fetch_array($result)) {
            $ausgabe[] = date('d.m H:00', $row['Time']);
        }
        mysqli_free_result($result);
    }
    mysqli_close($link);
    return json_encode($ausgabe);    
}

function getLabelIndex(){
    $link = getConnection();
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60;
    $query = 'SELECT Time FROM wetter_h WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    $ausgabe = array();
    /* Select queries return a resultset */
    if ($result = mysqli_query($link, $query)) {
        while ($row = mysqli_fetch_array($result)) {
            $ausgabe[] = date('d.m H:M', $row['Time']);
        }
        mysqli_free_result($result);
    }
    mysqli_close($link);
    return json_encode($ausgabe);    
}

//---------------------hourdata-------------------------------------
function getMaxTempHour() {
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_h ', 'Temperatur_MAX', $where);
}

function getMinTempHour() {
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_h ', 'Temperatur_Min', $where);
}

function getAvgTempHour() {
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_h ', 'Temperatur_AVG', $where);
}

function getPresHour() {
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_h ', 'Luftdruck', $where);
}

function getHumHour() {
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_h ', 'Luftfeuchtigkeit', $where);
}

function getSpeedHour() {
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_h ', 'Windgeschwindigkeit', $where);
}

function getRainHour() {
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_h ', 'Regen', $where);
}

//--------------------------------indexdata-------------------------------------------

function getTempIndex(){
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter ', ' Temperatur ', $where);
}

//--------------------------------daydata---------------------
function getHumDay() {
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24 * 7;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_d ', 'Luftfeuchtigkeit', $where);
}

function getRainDay() {
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24 * 7;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_d ', 'Regen', $where);
}

function getPresDay() {
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24 * 7;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_d ', 'Luftdruck', $where);
}

function getSpeedDay() {
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24 * 7;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_d ', 'Windgeschwindigkeit', $where);
}

function getMinTempDay() {
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24 * 7;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_d ', 'Temperatur_MIN', $where);
}

function getMaxTempDay() {
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24 * 7;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_d ', 'Temperatur_MAX', $where);
}

function getAvgTempDay() {
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24 * 7;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_d ', 'Temperatur_AVG', $where);
}

//-----------------datacustomstandart------------------

function getCustomAvgTempS(){
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_h ', 'Temperatur_AVG', $where);
}

function getCustomMaxTempS(){
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_h ', 'Temperatur_MAX', $where);
}

function getCustomMinTempS(){
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_h ', 'Temperatur_Min', $where);
}

function getCustomSpeedS(){
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_h ', 'Windgeschwindigkeit', $where);
}

function getCustomHumS(){
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_h ', 'Luftfeuchtigkeit', $where);
}

function getCustomPresS(){
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_h ', 'Luftdruck', $where);
}

function getCustomRainS(){
    $hour = date('y-m-d H:i:s');
    $hourStmp = strtotime($hour);
    $hourStmp2 = $hourStmp - 60 * 60 * 24;
    $where = ' WHERE Time BETWEEN ' . $hourStmp2 . ' AND ' . $hourStmp;
    return getData(' wetter_h ', 'Regen', $where);
}

//-----------datacustom-------------------------------------

function getCustomAvgTemp() {
    $where = ' WHERE Time BETWEEN ' . getBeginn() . ' AND ' . getEnd();
    return getData(' wetter_h ', 'Temperatur_AVG', $where);
}

function getCustomMaxTemp() {
    $where = ' WHERE Time BETWEEN ' . getBeginn() . ' AND ' . getEnd();
    return getData(' wetter_h ', 'Temperatur_MAX', $where);
}

function getCustomMinTemp() {
    $where = ' WHERE Time BETWEEN ' . getBeginn() . ' AND ' . getEnd();
    return getData(' wetter_h ', 'Temperatur_Min', $where);
}

function getCustomSpeed() {
    $where = ' WHERE Time BETWEEN ' . getBeginn() . ' AND ' . getEnd();
    return getData(' wetter_h ', 'Windgeschwindigkeit', $where);
}

function getCustomHum() {
    $where = ' WHERE Time BETWEEN ' . getBeginn() . ' AND ' . getEnd();
    return getData(' wetter_h ', 'Luftfeuchtigkeit', $where);
}

function getCustomPres() {
    $where = ' WHERE Time BETWEEN ' . getBeginn() . ' AND ' . getEnd();
    return getData(' wetter_h ', 'Luftdruck', $where);
}

function getCustomRain() {
    $where = ' WHERE Time BETWEEN ' . getBeginn() . ' AND ' . getEnd();
    return getData(' wetter_h ', 'Regen', $where);
}









































































































?>