<?php include './database.php'; ?>
<?php
function getCookieTemp(){
  return json_encode($_COOKIE["temp"]);
}

function getCookieSpeed(){
  return json_encode($_COOKIE["speed"]);
}

function getCookiePres(){
  return json_encode($_COOKIE["pres"]);
}
?>
<script>
  function createTemp() {
    var c = <?php echo getLastTemp(); ?>;
    c = parseFloat(c);
    var cookie = <?php echo getCookieTemp(); ?>;
    if(cookie == "fahr"){
      var f = ((c * 9 / 5) + 32);
      return f.toFixed(1) + "°F";
    }else if(cookie == "kel"){
      var k = parseFloat(c) + parseFloat(273.15);
      return k.toFixed(1) + "°K";
    }else{
      return c.toFixed(1) + "°C";
    }
  }

  function createSpeed(){
    var s = <?php echo getLastSpeed();?>;
    s = parseFloat(s);
    var cookie = <?php echo getCookieSpeed(); ?>;
    if(cookie == "mph"){
      var m = parseFloat(s * 0.621371);
      return m.toFixed(1) + " mph";
    }else if(cookie == "knoten"){
      var k = parseFloat(s * 0.539957);
      return k.toFixed(1) + " kn";
    }else{
      return s.toFixed(1) + " km/h";
    }
  }

  function createPres(){
    var pa = parseInt(<?php echo getLastPres();?>);
    var cookie = <?php echo getCookiePres(); ?>;
    if(cookie == "hpa"){
      var hpa = parseInt(pa / 100);
      return hpa + " hPa";
    }else if (cookie == "bar") {
      var bar = parseFloat(pa / 100000);
      return bar.toFixed(3) + " Bar";
    }else {
      return pa + " Pa";
    }
  }

  function getLiveData(){
    var hum = parseFloat(<?php echo getLastHum();?>).toFixed(1);
    document.getElementById('liveTemp').innerHTML='Temperatur: ' + createTemp();
    document.getElementById('liveSpeed').innerHTML='Windgeschwindigkeit: ' + createSpeed();
    document.getElementById('liveHumidity').innerHTML='Luftfeuchtigkeit: ' + hum + ' %';
    document.getElementById('livePressure').innerHTML='Luftdruck: ' + createPres();
  }

  function getHeadLiveData(){
    document.getElementById('temp').innerHTML=createTemp();
  }
</script>
