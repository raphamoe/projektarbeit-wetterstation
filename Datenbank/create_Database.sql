-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Erstellungszeit: 14. Feb 2019 um 08:55
-- Server-Version: 10.1.37-MariaDB-0+deb9u1
-- PHP-Version: 7.0.33-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `wetter`
--
CREATE DATABASE IF NOT EXISTS `wetter` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `wetter`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wetter`
--

CREATE TABLE `wetter` (
  `ID` int(11) NOT NULL,
  `Time` int(11) NOT NULL,
  `Temperatur` double DEFAULT NULL,
  `Luftdruck` int(11) DEFAULT NULL,
  `Luftfeuchtigkeit` double DEFAULT NULL,
  `Windgeschwindigkeit` double DEFAULT NULL,
  `Regen` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wetter_d`
--

CREATE TABLE `wetter_d` (
  `ID` int(11) NOT NULL,
  `Time` int(11) NOT NULL,
  `Temperatur_AVG` double DEFAULT NULL,
  `Temperatur_MAX` double DEFAULT NULL,
  `Temperatur_MIN` double DEFAULT NULL,
  `Luftdruck` double DEFAULT NULL,
  `Luftfeuchtigkeit` double DEFAULT NULL,
  `Windgeschwindigkeit` double DEFAULT NULL,
  `Regen` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wetter_h`
--

CREATE TABLE `wetter_h` (
  `ID` int(11) NOT NULL,
  `Time` int(11) NOT NULL,
  `Temperatur_AVG` double DEFAULT NULL,
  `Temperatur_MAX` double DEFAULT NULL,
  `Temperatur_Min` double DEFAULT NULL,
  `Luftdruck` double DEFAULT NULL,
  `Luftfeuchtigkeit` double DEFAULT NULL,
  `Windgeschwindigkeit` double DEFAULT NULL,
  `Regen` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `wetter`
--
ALTER TABLE `wetter`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `wetter_d`
--
ALTER TABLE `wetter_d`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `wetter_h`
--
ALTER TABLE `wetter_h`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `wetter`
--
ALTER TABLE `wetter`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23507;
--
-- AUTO_INCREMENT für Tabelle `wetter_d`
--
ALTER TABLE `wetter_d`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT für Tabelle `wetter_h`
--
ALTER TABLE `wetter_h`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=366;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
