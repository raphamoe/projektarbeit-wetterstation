package Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class MyDB {

    private String host, user, passwd;
    private Connection con;

    public MyDB(String host, String user, String passwd) {
        this.host = host;
        this.passwd = passwd;
        this.user = user;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println("Treiber konnte nicht geladen werden!");
            ex.printStackTrace();
        }
    }

    private void connect() {
        try {
            con = DriverManager.getConnection("jdbc:mariadb://" + host, user, passwd);
        } catch (SQLException ex) {
            System.out.println("Verbindug zum Server konnte nicht aufgebaut werden!");
            ex.printStackTrace();
        }
    }

    public String select(String table, String anzeige, String where) {
        StringBuilder sb = new StringBuilder();
        connect();
        try (Statement stmt = con.createStatement()) {
            ResultSet rs;
            if (where.isEmpty()) {
                rs = stmt.executeQuery("SELECT " + anzeige + " FROM " + table);
            } else {
                rs = stmt.executeQuery("SELECT " + anzeige + " FROM " + table + " " + where);
            }
            ResultSetMetaData rsmd = rs.getMetaData();
            while (rs.next()) {
                for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                    sb.append(rs.getString(i));
                    if (i != rsmd.getColumnCount()) {
                        sb.append(",");
                    }
                }
                sb.append(";");
            }
            rs.close();
        } catch (SQLException ex) {
            System.out.println("Select Anweisung fehlgeschlagen!");
            ex.printStackTrace();
        } finally {
            closeConnection();
        }

        return sb.toString();
    }

    public void insert(String table, String insert) {
        connect();
        try (Statement stmt = con.createStatement()) {
            stmt.executeUpdate("INSERT INTO " + table + " VALUES (" + insert + ")");
        } catch (SQLException ex) {
            System.out.println("Einfügen der Daten ist fehlgeschlagen!");
            ex.printStackTrace();
        } finally {
            closeConnection();
        }
    }

    private void closeConnection() {
        try {
            con.close();
        } catch (SQLException ex) {
            System.out.println("Schließen der Verbindung ist fehlgeschlagen");
            ex.printStackTrace();
        }
    }
}
