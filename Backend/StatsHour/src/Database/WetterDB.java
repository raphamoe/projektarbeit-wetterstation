package Database;

import java.util.Arrays;

public class WetterDB {

    private String host = "localhost", user = "admin", passwd = "ProjektSQL";

    private MyDB db;

    public WetterDB() {
        db = new MyDB(host + "/wetter", user, passwd);
    }

    //Wetter-Tabelle
    /**
     * Einfügen von Daten in die Wetter-Tabelle
     *
     * @param id
     * @param time
     * @param temp
     * @param druck
     * @param feuchtigkeit
     * @param geschwindigkeit
     * @param regen
     */
    public void addWetter(String id, String time, String temp, String druck, String feuchtigkeit, String geschwindigkeit, String regen) {
        db.insert("wetter", id + ", '" + time + "', '" + temp + "', '" + druck + "', '" + feuchtigkeit + "', '" + geschwindigkeit + "', '" + regen + "'");
    }

    /**
     * Ausgabe der ID aus der Wetter-Tabelle
     *
     * @param where
     * @return
     */
    public int[] getWetterID(String where) {
        return Arrays.stream(db.select("wetter", "ID", where).split(";")).mapToInt(Integer::parseInt).toArray();
    }

    /**
     * Ausgabe der MaximalenID aus der Wetter-Tabelle
     *
     * @return
     */
    public int getMaxWetterID() {
        return Integer.parseInt(db.select("wetter", "MAX(ID)", "").split(";")[0]);
    }

    /**
     * Ausgabe der Zeit aus der Wetter-Tabelle
     *
     * @param where
     * @return
     */
    public int[] getWetterTime(String where) {
        return Arrays.stream(db.select("wetter", "Time", where).split(";")).mapToInt(Integer::parseInt).toArray();
    }

    /**
     * Ausgabe der Temperatur aus der Wetter-Tabelle
     *
     * @param where
     * @return
     */
    public double[] getWetterTemp(String where) {
        return Arrays.stream(db.select("wetter", "Temperatur", where).split(";")).mapToDouble(Double::parseDouble).toArray();
    }

    /**
     * Ausgabe der Maximalentemperatur aus der Wetter-Tabelle
     *
     * @param where
     * @return
     */
    public double getMaxWetterTemp(String where) {
        return Double.parseDouble(db.select("wetter", "MAX(Temperatur)", where).split(";")[0]);
    }

    /**
     * Ausgabe der Minimalentemperatur aus der Wetter-Tabelle
     *
     * @param where
     * @return
     */
    public double getMinWetterTemp(String where) {
        return Double.parseDouble(db.select("wetter", "MIN(Temperatur)", where).split(";")[0]);
    }

    /**
     * Ausgabe des Durchschnittstemperatur aus der Wetter-Tabelle
     *
     * @param where
     * @return
     */
    public double getAvgWetterTemp(String where) {
        return Double.parseDouble(db.select("wetter", "AVG(Temperatur)", where).split(";")[0]);
    }

    /**
     * Ausgabe des Druck aus der Wetter-Tabelle
     *
     * @param where
     * @return
     */
    public int[] getWetterDruck(String where) {
        return Arrays.stream(db.select("wetter", "Luftdruck", where).split(";")).mapToInt(Integer::parseInt).toArray();
    }

    /**
     * Ausgabe des Durchschnittsdrucks aus der Wetter-Tabelle
     *
     * @param where
     * @return
     */
    public double getAvgWetterDruck(String where) {
        return Double.parseDouble(db.select("wetter", "AVG(Luftdruck)", where).split(";")[0]);
    }

    /**
     * Ausgabe der Feuchtigkeit aus der Wetter-Tabelle
     *
     * @param where
     * @return
     */
    public double[] getWetterFeuchtigkeit(String where) {
        return Arrays.stream(db.select("wetter", "Luftfeuchtigkeit", where).split(";")).mapToDouble(Double::parseDouble).toArray();
    }

    /**
     * Ausgabe der Durchschnittsfeuchtigkeit aus der Wetter-Tabelle
     *
     * @param where
     * @return
     */
    public double getAvgWetterFeuchtigkeit(String where) {
        return Double.parseDouble(db.select("wetter", "AVG(Luftfeuchtigkeit)", where).split(";")[0]);
    }

    /**
     * Ausgabe der Geschwindgkeit aus der Wetter-Tabelle
     *
     * @param where
     * @return
     */
    public double[] getWetterGeschwindigkeit(String where) {
        return Arrays.stream(db.select("wetter", "Windgeschwindigkeit", where).split(";")).mapToDouble(Double::parseDouble).toArray();
    }

    /**
     * Ausgabe der Durchschnittsgeschwindigkeit aus der Wetter-Tabelle
     *
     * @param where
     * @return
     */
    public double getAvgWetterGeschwindigkeit(String where) {
        return Double.parseDouble(db.select("wetter", "AVG(Windgeschwindigkeit)", where).split(";")[0]);
    }

    /**
     * Ausgabe des Regens aus der Wetter-Tabelle
     *
     * @param where
     * @return
     */
    public int[] getWetterRegen(String where) {
        return Arrays.stream(db.select("wetter", "Regen", where).split(";")).mapToInt(Integer::parseInt).toArray();
    }

    /**
     * Ausgabe der ganzen Wetter-Tabelle
     *
     * @param where
     * @return
     */
    public String getWetterTabelle(String where) {
        return db.select("wetter", "*", where);
    }

    //Wetter_htabelle
    /**
     * Einfügen von Daten in die Wetter_h-Tabelle
     *
     * @param id
     * @param time
     * @param tempAvg
     * @param tempMax
     * @param tempMin
     * @param druck
     * @param feutigkeit
     * @param geschwindigkeit
     * @param regen
     */
    public void addWetter_h(String id, String time, String tempAvg, String tempMax, String tempMin, String druck, String feutigkeit, String geschwindigkeit, String regen) {
        db.insert("wetter_h", id + ", '" + time + "', '" + tempAvg + "', '" + tempMax + "', '" + tempMin + "', '" + druck + "', '" + feutigkeit + "', '" + geschwindigkeit + "', '" + regen + "'");
    }

    /**
     * Ausgabe der ID aus der Wetter_h-Tabelle
     *
     * @param where
     * @return
     */
    public int[] getWetter_hID(String where) {
        return Arrays.stream(db.select("wetter_h", "ID", where).split(";")).mapToInt(Integer::parseInt).toArray();
    }

    /**
     * Ausgabe der MaximalenID aus der Wetter_h-Tabelle
     *
     * @return
     */
    public int getMaxWetter_hID() {
        return Integer.valueOf(db.select("wetter_h", "MAX(ID)", "").split(";")[0]);
    }

    /**
     * Ausgabe der Zeit aus der Wetter_h-Tabelle
     *
     * @param where
     * @return
     */
    public int[] getWetter_hTime(String where) {
        return Arrays.stream(db.select("wetter_h", "Time", where).split(";")).mapToInt(Integer::parseInt).toArray();
    }

    /**
     * Ausgabe der Temperatur-Durchschnitt aus der Wetter_h-Tabelle
     *
     * @param where
     * @return
     */
    public double[] getWetter_hTempAvg(String where) {
        return Arrays.stream(db.select("wetter_h", "Temperatur_AVG", where).split(";")).mapToDouble(Double::parseDouble).toArray();
    }

    /**
     * Ausgabe und berechnung des Druchschnitts für die Durchschnittstemperautr
     * aus der Wetter_h-Tabelle
     *
     * @param where
     * @return
     */
    public double getAvgWetter_hTempAvg(String where) {
        return Double.parseDouble(db.select("wetter_h", "AVG(Temperatur_AVG)", where).split(";")[0]);
    }

    /**
     * Ausgabe der Maximaltemperatur aus der Wetter_h-Tabelle
     *
     * @param where
     * @return
     */
    public double[] getWetter_hTempMax(String where) {
        return Arrays.stream(db.select("wetter_h", "Temperatur_Max", where).split(";")).mapToDouble(Double::parseDouble).toArray();
    }

    /**
     * Ausgabe und berechnung des Maximalwerts für die Maximaltemperatur aus der
     * Wetter_h-Tabelle
     *
     * @param where
     * @return
     */
    public double getMaxWetter_hTempMax(String where) {
        return Double.parseDouble(db.select("wetter_h", "MAX(Temperatur_MAX)", where).split(";")[0]);
    }

    /**
     * Ausgabe der Minmaltemperatur aus der Wetter_h-Tabelle
     *
     * @param where
     * @return
     */
    public double[] getWetter_hTempMin(String where) {
        return Arrays.stream(db.select("wetter_h", "Temperatur_Min", where).split(";")).mapToDouble(Double::parseDouble).toArray();
    }

    /**
     * Ausgabe und berechnung des Minimalwertes für die Minmaltemperatur aus der
     * Wetter_h-Tabelle
     *
     * @param where
     * @return
     */
    public double getMinWetter_hTempMin(String where) {
        return Double.parseDouble(db.select("wetter_h", "MIN(Temperatur_MIN)", where).split(";")[0]);
    }

    /**
     * Ausgabe des Drucks aus der Wetter_h-Tabelle
     *
     * @param where
     * @return
     */
    public int[] getWetter_hDruck(String where) {
        return Arrays.stream(db.select("wetter_h", "Luftdruck", where).split(";")).mapToInt(Integer::parseInt).toArray();
    }

    /**
     * Ausgabe des Druchschnittsdrucks aus der Wetter_h-Tabelle
     *
     * @param where
     * @return
     */
    public double getAvgWetter_hDruck(String where) {
        return Double.parseDouble(db.select("wetter_h", "AVG(Luftdruck)", where).split(";")[0]);
    }

    /**
     * Ausgabe der Feuchtigkeit aus der Wetter_h Tabelle
     *
     * @param where
     * @return
     */
    public double[] getWetter_hFeuchtigkeit(String where) {
        return Arrays.stream(db.select("wetter_h", "Luftfeuchtigkeit", where).split(";")).mapToDouble(Double::parseDouble).toArray();
    }

    /**
     * Asugabe der Druchschnittsfechtigketi aus der Wetter_h-Tabelle
     *
     * @param where
     * @return
     */
    public double getAvgWetter_hFeuchtigkeit(String where) {
        return Integer.parseInt(db.select("wetter_h", "AVG(Luftfeuchtigkeit)", where).split(";")[0]);
    }

    /**
     * Ausgabe der Geschwindigkeit aus der Wetter_h-Tabelle
     *
     * @param where
     * @return
     */
    public double[] getWetter_hGeschwindigkeit(String where) {
        return Arrays.stream(db.select("wetter_h", "Windgeschwindigkeit", where).split(";")).mapToDouble(Double::parseDouble).toArray();
    }

    /**
     * Ausgabe der Durchschnittsgeschwindigkeit aus der Wetter_h-Tabelle
     *
     * @param where
     * @return
     */
    public double getAvgWetter_hGeschwindigkeit(String where) {
        return Double.parseDouble(db.select("wetter_h", "AVG(Windgeschwindigkeit)", where).split(";")[0]);
    }

    /**
     * Ausgabe des Regens aus der Wetter_h-Tabelle
     *
     * @param where
     * @return
     */
    public int[] getWetter_hRegen(String where) {
        return Arrays.stream(db.select("wetter_h", "Regen", where).split(";")).mapToInt(Integer::parseInt).toArray();
    }

    /**
     * Ausgabe des Durchschnittsregen aus der Wetter_h-Tabelle
     *
     * @param where
     * @return
     */
    public double getAvgWetter_hRegen(String where) {
        return Double.parseDouble(db.select("wetter_h", "Regen", where).split(";")[0]);
    }

    /**
     * Ausgabe der Sum des Regens aus der Wetter_h-Tabelle
     * @param where
     * @return 
     */
    public int getSumWetter_hRegen(String where) {
        return Integer.parseInt(db.select("wetter_h", "SUM(Regen)", where).split(";")[0]);
    }

    /**
     * Ausgabe der ganzen Wetter_h-Tabelle
     * @param where
     * @return 
     */
    public String getWetter_hTabelle(String where) {
        return db.select("wetter_h", "*", where);
    }

    //Wettr_dtabelle
    /**
     * Einfügen von Daten in der Wetter_d-Tabelle
     * @param id
     * @param time
     * @param tempAvg
     * @param tempMax
     * @param tempMin
     * @param druck
     * @param feutigkeit
     * @param geschwindigkeit
     * @param regen 
     */
    public void addWetter_d(String id, String time, String tempAvg, String tempMax, String tempMin, String druck, String feutigkeit, String geschwindigkeit, String regen) {
        db.insert("wetter_d", id + ", '" + time + "', '" + tempAvg + "', '" + tempMax + "', '" + tempMin + "', '" + druck + "', '" + feutigkeit + "', '" + geschwindigkeit + "', '" + regen + "'");
    }

    /**
     * Ausgabe der MaximalenID aus der Wetter_d-Tabelle
     * @return 
     */
    public int getMaxWetter_dID() {
        return Integer.parseInt(db.select("wetter_d", "MAX(ID)", "").split(";")[0]);
    }

    /**
     * Ausgab der ganzen Wetter_d-Tabelle
     * @param where
     * @return 
     */
    public String getWetter_dTabelle(String where) {
        return db.select("wetter_d", "*", where);
    }
}
