package Main;

import Database.WetterDB;
import java.util.GregorianCalendar;

public class Stats {

    private int[] time;

    public Stats(String[] args) {
        if (args.length == 0) {
            System.err.println("Bitte einen Parameter übergeben\nd = Taeglich\nh = Stuendlich");
        } else {
            switch (args[0]) {
                case "d":
                    time = createTimeDay();
                    createStatsDay();
                    break;
                case "h":
                    time = createTimeHour();
                    createStatsHour();
                    break;
                default:
                    System.err.println("Ungültiger Parameter\nd = Taeglich\nh = Stuendlich");
                    break;
            }
        }
    }

    // Erzeugt 2 Timestamps: Den Anfang einer Stunde, und eine Sekunde vor Ende der Stunde
    private int[] createTimeHour() {
        int[] res = new int[2];
        GregorianCalendar c = new GregorianCalendar();
        c.add(GregorianCalendar.HOUR_OF_DAY, -1);
        c.set(GregorianCalendar.MINUTE, 0);
        c.set(GregorianCalendar.SECOND, 0);
        res[0] = (int) (c.getTimeInMillis() / 1000);

        c.set(GregorianCalendar.MINUTE, 59);
        c.set(GregorianCalendar.SECOND, 59);
        res[1] = (int) (c.getTimeInMillis() / 1000);
        return res;
    }

    // Erzeugt 2 Timestamps: Den Anfang eines Tages, und eine Sekunde vor Ende des Tages
    private int[] createTimeDay() {
        int[] res = new int[2];
        GregorianCalendar c = new GregorianCalendar();
        c.add(GregorianCalendar.DAY_OF_MONTH, -1);
        c.set(GregorianCalendar.HOUR_OF_DAY, 0);
        c.set(GregorianCalendar.MINUTE, 0);
        c.set(GregorianCalendar.SECOND, 0);
        res[0] = (int) (c.getTimeInMillis() / 1000);

        c.set(GregorianCalendar.HOUR_OF_DAY, 23);
        c.set(GregorianCalendar.MINUTE, 59);
        c.set(GregorianCalendar.SECOND, 59);
        res[1] = (int) (c.getTimeInMillis() / 1000);
        return res;
    }

    // Liest die durchschnittlichen Werte eines ganzen Tages aus der Datenbank aus und erstellt einen neuen Eintrag aus den Werten in der täglichen Datenbank
    private void createStatsDay() {
        int stamp = (int) (System.currentTimeMillis() / 1000);
        WetterDB db = new WetterDB();
        String where = "WHERE Time BETWEEN " + time[0] + " AND " + time[1];
        double avgTemp = db.getAvgWetter_hTempAvg(where);
        double maxTemp = db.getMaxWetter_hTempMax(where);
        double minTemp = db.getMinWetter_hTempMin(where);
        double avgDruck = db.getAvgWetter_hDruck(where);
        double avgFeuchtigkeit = db.getAvgWetter_hFeuchtigkeit(where);
        double avgGeschwindigkeit = db.getAvgWetter_hGeschwindigkeit(where);
        int regenTime = db.getSumWetter_hRegen(where);
        db.addWetter_d(null, String.valueOf(stamp), String.valueOf(avgTemp), String.valueOf(maxTemp), String.valueOf(minTemp), String.valueOf(avgDruck), String.valueOf(avgFeuchtigkeit), String.valueOf(avgGeschwindigkeit), String.valueOf(regenTime));
    }

    // Liest die durchschnittlichen Werte einer Stunde aus der Datenbank aus und erstellt einen neuen Eintrag aus den Werten in der stündlichen Datenbank
    private void createStatsHour() {
        int stamp = (int) (System.currentTimeMillis() / 1000);
        WetterDB db = new WetterDB();
        String where = "WHERE Time BETWEEN " + time[0] + " AND " + time[1];
        double avgTemp = db.getAvgWetterTemp(where);
        double maxTemp = db.getMaxWetterTemp(where);
        double minTemp = db.getMinWetterTemp(where);
        double avgDruck = db.getAvgWetterDruck(where);
        double avgFeuchtigkeit = db.getAvgWetterFeuchtigkeit(where);
        double avgGeschwindigkeit = db.getAvgWetterGeschwindigkeit(where);
        int[] regen = db.getWetterRegen(where);
        int timeRegen = 0;
        for (int i : regen) {
            if (i != 0) {
                timeRegen++;
            }
        }
        db.addWetter_h(null, String.valueOf(stamp), String.valueOf(avgTemp), String.valueOf(maxTemp), String.valueOf(minTemp), String.valueOf(avgDruck), String.valueOf(avgFeuchtigkeit), String.valueOf(avgGeschwindigkeit), String.valueOf(timeRegen));
    }

    // Führt das Programm aus.
    public static void main(String[] args) {
        new Stats(args);
    }

}
