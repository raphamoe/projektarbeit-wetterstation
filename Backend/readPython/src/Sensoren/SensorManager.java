package Sensoren;

import java.util.logging.Level;
import java.util.logging.Logger;

public class SensorManager {

    // Jeder Sensor führt das Angegebene Programm in Python aus
    Sensor barometer = new Sensor("python /home/pi/pythonScripts/barometer.py");
    Sensor feuchtigkeit = new Sensor("python /home/pi/pythonScripts/feuchtigkeit.py");
    Sensor regen = new Sensor("python /home/pi/pythonScripts/regensensor.py");
    Sensor wind = new Sensor("python /home/pi/pythonScripts/windsensor.py");
    Sensor temp = new Sensor("python /home/pi/pythonScripts/temperatur.py");

    // Lufrdruck lesen
    public String readLuftdruck() {
        barometer.readData();
        try {
            return barometer.getOutput();
        } catch (InterruptedException ex) {
            Logger.getLogger(SensorManager.class.getName()).log(Level.SEVERE, null, ex);
            return "0";
        }
    }

    // Temperatur lesen
    public String readTemperatur() {
        temp.readData();
        try {
            return temp.getOutput();
        } catch (InterruptedException ex) {
            Logger.getLogger(SensorManager.class.getName()).log(Level.SEVERE, null, ex);
            return "0";
        }
    }

    // Regen abfragen
    public String isRaining() {
        regen.readData();
        try {
            return regen.getOutput();
        } catch (InterruptedException ex) {
            Logger.getLogger(SensorManager.class.getName()).log(Level.SEVERE, null, ex);
            return "0";
        }
    }

    // Windgeschwindigkeit abfragen
    public String readWindG() {
        wind.readData();
        try {
            return wind.getOutput();
        } catch (InterruptedException ex) {
            Logger.getLogger(SensorManager.class.getName()).log(Level.SEVERE, null, ex);
            return "0";
        }
    }

    // Luftfeuchtigkeit lesen
    public String readFeuchtigkeit() {
        feuchtigkeit.readData();
        try {
            return feuchtigkeit.getOutput();
        } catch (InterruptedException ex) {
            Logger.getLogger(SensorManager.class.getName()).log(Level.SEVERE, null, ex);
            return "0";
        }
    }
}
