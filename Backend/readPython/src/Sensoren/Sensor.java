package Sensoren;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Sensor {

    // Klasse für einen Sensor.
    // Ein Sensor ist im Bezug dieses Programmes ein Prozess des Betriebssystems, welche ein Python Script ausführt
    // Jeder Prozess läuft in seinem eigenen Thread

    private final String cmd;
    private String output = "";
    private Thread t;

    public Sensor(String cmd) {
        this.cmd = cmd;
    }

    // Liest die Daten eines Sensors in einem eigenen Thread
    public void readData() {
        t = new Thread() {
            @Override
            public void run() {
                try {
                    String s;
                    Process p = Runtime.getRuntime().exec(cmd);
                    BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    // read the output from the command
                    if ((s = stdInput.readLine()) != null) {
                        buildString(s);
                    }
                } catch (IOException ex) {
                    System.out.println("Error executing script.");
                }
            }
        };
        t.start();
    }

    private void buildString(String add) {
        output = add;
    }

    // Wartet auf das Ende des Threads bevor die Ausgabe erfolgt
    public String getOutput() throws InterruptedException {
        while (t.isAlive()) {
            // warte 50 Milisekunden
            Thread.sleep(50);
        }
        return output;
    }
}
