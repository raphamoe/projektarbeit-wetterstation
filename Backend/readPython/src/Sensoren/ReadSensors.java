package Sensoren;

public class ReadSensors {

    // Eine Klasse welche die Daten der Sensoren in einer bequemen Methode sammelt und übergibt

   private SensorManager SM;

    public ReadSensors() {
        SM = new SensorManager();
    }

    // Liest die Dateien der 5 Sensoren und übergibt diese als String[] Array
    public String[] getVals() {
        double feuchtMin = 0, tempMin = 0, druckMin = 0, windMin = 0;
        int rainMin = 0;
        int count = 0;
        // Fragt die Sensoren 6 Mal ab, dauert ca. 1 Minute 
        while (count < 6) {
                feuchtMin += Double.parseDouble(SM.readFeuchtigkeit());
                tempMin += Double.parseDouble(SM.readTemperatur());
                druckMin += Double.parseDouble(SM.readLuftdruck());             
                rainMin += Integer.parseInt(SM.isRaining());
                windMin += Double.parseDouble(SM.readWindG());
                count++;
        }
        String[] vals = new String[5];
        vals[0] = String.valueOf(tempMin / 6);
        vals[1] = String.valueOf(druckMin / 6);
        vals[2] = String.valueOf(feuchtMin / 6);
        vals[3] = String.valueOf(windMin / 6);
        vals[4] = String.valueOf(rainMin);
        return vals;
    }
}
