package mainProg;

import Sensoren.*;
import Database.WetterDB;


public class main {

    // Main Methode sammelt die Sensorwerte des SensorManagers und übergibt diese an die Datenbank 
    // Wird jede Minute ausgeführt via crontab auf Linux OS
    public static void main(String[] args) {
        String time = String.valueOf(System.currentTimeMillis() / 1000L);
        ReadSensors RS = new ReadSensors();
        WetterDB DB = new WetterDB();   
        String[] vals = RS.getVals();
        DB.addWetter(null, time, vals[0], vals[1], vals[2], vals[3], vals[4]);
    }
}
