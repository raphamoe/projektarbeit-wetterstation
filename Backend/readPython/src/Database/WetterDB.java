package Database ;

public class WetterDB {

    // Diese Klasse verfügt über Konkrete SQL Anweisungen zum Auswählen und hinzufügen von Daten
    // d = day (tägliche Daten)
    // h = hour (stündliche Daten)

    private String host = "localhost", user = "admin", passwd = "ProjektSQL";

    private MyDB db;

    public WetterDB() {
        db = new MyDB(host + "/wetter", user, passwd);
    }

    public void addWetter(String id, String time, String temp, String druck, String feuchtigkeit, String geschwindigkeit, String regen) {
        db.insert("wetter",id + ", '" + time + "', '" + temp + "', '" + druck + "', '" + feuchtigkeit + "', '" + geschwindigkeit + "', '" + regen + "'");
    }

    public String getWetterID(String where) {
        return db.select("wetter", "ID", where);
    }

    public int getMaxWetterID() {
        return Integer.parseInt(db.select("wetter", "MAX(ID)", "").split(";")[0]);
    }

    public String getWetterTime(String where) {
        return db.select("wetter", "Time", where);
    }

    public String getWetterTemp(String where) {
        return db.select("wetter", "Temperatur", where);
    }

    public String getWetterDruck(String where) {
        return db.select("wetter", "Luftdruck", where);
    }

    public String getWetterFeuchtigkeit(String where) {
        return db.select("wetter", "Luftfeuchtigkeit", where);
    }

    public String getWetterGeschwindigkeit(String where) {
        return db.select("wetter", "Windgeschwindigkeit", where);
    }

    public String getWetterRegen(String where) {
        return db.select("wetter", "Regen", where);
    }

    public String getWetterTabelle(String where) {
        return db.select("wetter", "*", where);
    }

    public void addWetter_h(String id, String time, String tempAvg, String tempMax, String tempMin, String druck, String feutigkeit, String geschwindigkeit, String regen) {
        db.insert("wetter_h", "'" + id + "', '" + time + "', '" + tempAvg + "', '" + tempMax + "', '" + tempMin + "', '" + druck + "', '" + feutigkeit + "', '" + geschwindigkeit + "', '" + regen + "'");
    }

    public String getWetter_hID(String where) {
        return db.select("wetter_h", "ID", where);
    }

    public int getMaxWetter_hID() {
        return Integer.valueOf(db.select("wetter_h", "MAX(ID)", "").split(";")[0]);
    }

    public String getWetter_hTime(String where) {
        return db.select("wetter_h", "Time", where);
    }

    public String getWetter_hTempAvg(String where) {
        return db.select("wetter_h", "Temperatur_AVG", where);
    }

    public String getWetter_hTempMax(String where) {
        return db.select("wetter_h", "Temperatur_Max", where);
    }

    public String getWetter_hTempMin(String where) {
        return db.select("wetter_h", "Temperatur_Min", where);
    }

    public String getWetter_hDruck(String where) {
        return db.select("wetter_h", "Luftdruck", where);
    }

    public String getWetter_hFeuchtigkeit(String where) {
        return db.select("wetter_h", "Luftfeuchtigkeit", where);
    }

    public String getWetter_hGeschwindigkeit(String where) {
        return db.select("wetter_h", "Windgeschwindigkeit", where);
    }

    public String getWetter_hRegen(String where) {
        return db.select("wetter_h", "Regen", where);
    }

    public String getWetter_hTabelle(String where) {
        return db.select("wetter_h", "*", where);
    }

    public void addWetter_d(String id, String time, String tempAvg, String tempMax, String tempMin, String druck, String feutigkeit, String geschwindigkeit, String regen) {
        db.insert("wetter_d", "'" + id + "', '" + time + "', '" + tempAvg + "', '" + tempMax + "', '" + tempMin + "', '" + druck + "', '" + feutigkeit + "', '" + geschwindigkeit + "', '" + regen + "'");
    }

    public int getMaxWetter_dID() {
        return Integer.parseInt(db.select("wetter_d", "MAX(ID)", "").split(";")[0]);
    }

    public String getWetter_dTabelle(String where) {
        return db.select("wetter_d", "*", where);
    }
}
